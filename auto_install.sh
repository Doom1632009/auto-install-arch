#!/bin/bash

# Set variables for the installation process
hostname="myarch"
username="myuser"
password="mypassword"

# Partition the disk (replace /dev/sda with your disk)
parted -s /dev/sda mklabel gpt  # Create GPT partition table
parted -s /dev/sda mkpart primary ext4 1MiB 100%  # Create root partition (full size)

# Format and mount the partition
mkfs.ext4 /dev/sda1  # Format the root partition as ext4
mount /dev/sda1 /mnt  # Mount root partition to /mnt

# Set timezone and language
ln -sf /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime  # Set the timezone to Asia/Ho_Chi_Minh
hwclock --systohc  # Sync hardware clock with system clock
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen  # Uncomment en_US.UTF-8 locale
locale-gen  # Generate the locales
echo "LANG=en_US.UTF-8" > /etc/locale.conf  # Set the language
echo "KEYMAP=us" > /etc/vconsole.conf  # Set the keyboard layout to US

# Set the hostname
echo "$hostname" > /etc/hostname  # Set the hostname
echo "127.0.0.1    localhost" >> /etc/hosts  # Add localhost entry to hosts file
echo "::1          localhost" >> /etc/hosts  # Add ipv6 localhost entry to hosts file
echo "127.0.1.1    $hostname.localdomain   $hostname" >> /etc/hosts  # Add hostname entry to hosts file

# Configure networking
systemctl enable dhcpcd.service  # Enable DHCP for network configuration

# Install the base system
pacstrap /mnt base linux linux-firmware

# Generate fstab
genfstab -U /mnt >> /mnt/etc/fstab

# Chroot into the installed system
arch-chroot /mnt <<EOF

# Set timezone and language
ln -sf /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime
hwclock --systohc
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "KEYMAP=us" > /etc/vconsole.conf

# Set the hostname
echo "$hostname" > /etc/hostname
echo "127.0.0.1    localhost" >> /etc/hosts
echo "::1          localhost" >> /etc/hosts
echo "127.0.1.1    $hostname.localdomain   $hostname" >> /etc/hosts

# Configure networking
systemctl enable dhcpcd.service

# Install and configure the boot loader (GRUB)
pacman -Sy --noconfirm grub efibootmgr
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=ArchLinux
grub-mkconfig -o /boot/grub/grub.cfg

# Install and configure NetworkManager
pacman -Sy --noconfirm networkmanager
systemctl enable NetworkManager.service

# Install and configure Sway window manager
pacman -Sy --noconfirm sway
pacman -Sy --noconfirm waybar
pacman -Sy --noconfirm bemenu
pacman -Sy --noconfirm foot

# Create a user and set password
useradd -m -G wheel -s /bin/bash $username
echo "$username:$password" | chpasswd
sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

# Update the system
pacman -Syu --noconfirm

EOF

# Unmount partitions
umount -R /mnt

# End of Arch Linux installation process
echo "Arch Linux installation process completed!"